package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MemberDislike;
import com.ruoyi.app.service.IMemberDislikeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 不喜欢Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/memberDislike")
public class MemberDislikeController extends BaseController
{
    private String prefix = "app/memberDislike";

    @Autowired
    private IMemberDislikeService memberDislikeService;

    @RequiresPermissions("app:memberDislike:view")
    @GetMapping()
    public String memberDislike()
    {
        return prefix + "/memberDislike";
    }

    /**
     * 查询不喜欢列表
     */
    @RequiresPermissions("app:memberDislike:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberDislike memberDislike)
    {
        startPage();
        List<MemberDislike> list = memberDislikeService.selectMemberDislikeList(memberDislike);
        return getDataTable(list);
    }

    /**
     * 导出不喜欢列表
     */
    @RequiresPermissions("app:memberDislike:export")
    @Log(title = "不喜欢", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberDislike memberDislike)
    {
        List<MemberDislike> list = memberDislikeService.selectMemberDislikeList(memberDislike);
        ExcelUtil<MemberDislike> util = new ExcelUtil<MemberDislike>(MemberDislike.class);
        return util.exportExcel(list, "memberDislike");
    }

    /**
     * 新增不喜欢
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存不喜欢
     */
    @RequiresPermissions("app:memberDislike:add")
    @Log(title = "不喜欢", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberDislike memberDislike)
    {
        return toAjax(memberDislikeService.insertMemberDislike(memberDislike));
    }

    /**
     * 修改不喜欢
     */
    @GetMapping("/edit/{dislikeId}")
    public String edit(@PathVariable("dislikeId") Long dislikeId, ModelMap mmap)
    {
        MemberDislike memberDislike = memberDislikeService.selectMemberDislikeById(dislikeId);
        mmap.put("memberDislike", memberDislike);
        return prefix + "/edit";
    }

    /**
     * 修改保存不喜欢
     */
    @RequiresPermissions("app:memberDislike:edit")
    @Log(title = "不喜欢", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberDislike memberDislike)
    {
        return toAjax(memberDislikeService.updateMemberDislike(memberDislike));
    }

    /**
     * 删除不喜欢
     */
    @RequiresPermissions("app:memberDislike:remove")
    @Log(title = "不喜欢", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberDislikeService.deleteMemberDislikeByIds(ids));
    }
}
