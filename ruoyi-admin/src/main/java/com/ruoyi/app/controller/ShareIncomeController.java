package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.ShareIncome;
import com.ruoyi.app.service.IShareIncomeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 分享新用户收入Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/shareIncome")
public class ShareIncomeController extends BaseController
{
    private String prefix = "app/shareIncome";

    @Autowired
    private IShareIncomeService shareIncomeService;

    @RequiresPermissions("app:shareIncome:view")
    @GetMapping()
    public String shareIncome()
    {
        return prefix + "/shareIncome";
    }

    /**
     * 查询分享新用户收入列表
     */
    @RequiresPermissions("app:shareIncome:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShareIncome shareIncome)
    {
        startPage();
        List<ShareIncome> list = shareIncomeService.selectShareIncomeList(shareIncome);
        return getDataTable(list);
    }

    /**
     * 导出分享新用户收入列表
     */
    @RequiresPermissions("app:shareIncome:export")
    @Log(title = "分享新用户收入", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShareIncome shareIncome)
    {
        List<ShareIncome> list = shareIncomeService.selectShareIncomeList(shareIncome);
        ExcelUtil<ShareIncome> util = new ExcelUtil<ShareIncome>(ShareIncome.class);
        return util.exportExcel(list, "shareIncome");
    }

    /**
     * 新增分享新用户收入
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存分享新用户收入
     */
    @RequiresPermissions("app:shareIncome:add")
    @Log(title = "分享新用户收入", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShareIncome shareIncome)
    {
        return toAjax(shareIncomeService.insertShareIncome(shareIncome));
    }

    /**
     * 修改分享新用户收入
     */
    @GetMapping("/edit/{shareIncomeId}")
    public String edit(@PathVariable("shareIncomeId") String shareIncomeId, ModelMap mmap)
    {
        ShareIncome shareIncome = shareIncomeService.selectShareIncomeById(shareIncomeId);
        mmap.put("shareIncome", shareIncome);
        return prefix + "/edit";
    }

    /**
     * 修改保存分享新用户收入
     */
    @RequiresPermissions("app:shareIncome:edit")
    @Log(title = "分享新用户收入", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShareIncome shareIncome)
    {
        return toAjax(shareIncomeService.updateShareIncome(shareIncome));
    }

    /**
     * 删除分享新用户收入
     */
    @RequiresPermissions("app:shareIncome:remove")
    @Log(title = "分享新用户收入", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shareIncomeService.deleteShareIncomeByIds(ids));
    }
}
