package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.NoteType;
import com.ruoyi.app.service.INoteTypeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游记类型Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/noteType")
public class NoteTypeController extends BaseController
{
    private String prefix = "app/noteType";

    @Autowired
    private INoteTypeService noteTypeService;

    @RequiresPermissions("app:noteType:view")
    @GetMapping()
    public String noteType()
    {
        return prefix + "/noteType";
    }

    /**
     * 查询游记类型列表
     */
    @RequiresPermissions("app:noteType:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NoteType noteType)
    {
        startPage();
        List<NoteType> list = noteTypeService.selectNoteTypeList(noteType);
        return getDataTable(list);
    }

    /**
     * 导出游记类型列表
     */
    @RequiresPermissions("app:noteType:export")
    @Log(title = "游记类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NoteType noteType)
    {
        List<NoteType> list = noteTypeService.selectNoteTypeList(noteType);
        ExcelUtil<NoteType> util = new ExcelUtil<NoteType>(NoteType.class);
        return util.exportExcel(list, "noteType");
    }

    /**
     * 新增游记类型
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存游记类型
     */
    @RequiresPermissions("app:noteType:add")
    @Log(title = "游记类型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NoteType noteType)
    {
        return toAjax(noteTypeService.insertNoteType(noteType));
    }

    /**
     * 修改游记类型
     */
    @GetMapping("/edit/{noteTypeId}")
    public String edit(@PathVariable("noteTypeId") Long noteTypeId, ModelMap mmap)
    {
        NoteType noteType = noteTypeService.selectNoteTypeById(noteTypeId);
        mmap.put("noteType", noteType);
        return prefix + "/edit";
    }

    /**
     * 修改保存游记类型
     */
    @RequiresPermissions("app:noteType:edit")
    @Log(title = "游记类型", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NoteType noteType)
    {
        return toAjax(noteTypeService.updateNoteType(noteType));
    }

    /**
     * 删除游记类型
     */
    @RequiresPermissions("app:noteType:remove")
    @Log(title = "游记类型", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(noteTypeService.deleteNoteTypeByIds(ids));
    }
}
