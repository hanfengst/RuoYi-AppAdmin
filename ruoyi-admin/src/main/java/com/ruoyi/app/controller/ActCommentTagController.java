package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.ActCommentTag;
import com.ruoyi.app.service.IActCommentTagService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 活动评价标签Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/actCommentTag")
public class ActCommentTagController extends BaseController
{
    private String prefix = "app/actCommentTag";

    @Autowired
    private IActCommentTagService actCommentTagService;

    @RequiresPermissions("app:actCommentTag:view")
    @GetMapping()
    public String actCommentTag()
    {
        return prefix + "/actCommentTag";
    }

    /**
     * 查询活动评价标签列表
     */
    @RequiresPermissions("app:actCommentTag:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ActCommentTag actCommentTag)
    {
        startPage();
        List<ActCommentTag> list = actCommentTagService.selectActCommentTagList(actCommentTag);
        return getDataTable(list);
    }

    /**
     * 导出活动评价标签列表
     */
    @RequiresPermissions("app:actCommentTag:export")
    @Log(title = "活动评价标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ActCommentTag actCommentTag)
    {
        List<ActCommentTag> list = actCommentTagService.selectActCommentTagList(actCommentTag);
        ExcelUtil<ActCommentTag> util = new ExcelUtil<ActCommentTag>(ActCommentTag.class);
        return util.exportExcel(list, "actCommentTag");
    }

    /**
     * 新增活动评价标签
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存活动评价标签
     */
    @RequiresPermissions("app:actCommentTag:add")
    @Log(title = "活动评价标签", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ActCommentTag actCommentTag)
    {
        return toAjax(actCommentTagService.insertActCommentTag(actCommentTag));
    }

    /**
     * 修改活动评价标签
     */
    @GetMapping("/edit/{tagId}")
    public String edit(@PathVariable("tagId") Long tagId, ModelMap mmap)
    {
        ActCommentTag actCommentTag = actCommentTagService.selectActCommentTagById(tagId);
        mmap.put("actCommentTag", actCommentTag);
        return prefix + "/edit";
    }

    /**
     * 修改保存活动评价标签
     */
    @RequiresPermissions("app:actCommentTag:edit")
    @Log(title = "活动评价标签", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ActCommentTag actCommentTag)
    {
        return toAjax(actCommentTagService.updateActCommentTag(actCommentTag));
    }

    /**
     * 删除活动评价标签
     */
    @RequiresPermissions("app:actCommentTag:remove")
    @Log(title = "活动评价标签", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(actCommentTagService.deleteActCommentTagByIds(ids));
    }
}
