package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.RecommentMember;
import com.ruoyi.app.service.IRecommentMemberService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 推荐会员Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/recommentMember")
public class RecommentMemberController extends BaseController
{
    private String prefix = "app/recommentMember";

    @Autowired
    private IRecommentMemberService recommentMemberService;

    @RequiresPermissions("app:recommentMember:view")
    @GetMapping()
    public String recommentMember()
    {
        return prefix + "/recommentMember";
    }

    /**
     * 查询推荐会员列表
     */
    @RequiresPermissions("app:recommentMember:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(RecommentMember recommentMember)
    {
        startPage();
        List<RecommentMember> list = recommentMemberService.selectRecommentMemberList(recommentMember);
        return getDataTable(list);
    }

    /**
     * 导出推荐会员列表
     */
    @RequiresPermissions("app:recommentMember:export")
    @Log(title = "推荐会员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(RecommentMember recommentMember)
    {
        List<RecommentMember> list = recommentMemberService.selectRecommentMemberList(recommentMember);
        ExcelUtil<RecommentMember> util = new ExcelUtil<RecommentMember>(RecommentMember.class);
        return util.exportExcel(list, "recommentMember");
    }

    /**
     * 新增推荐会员
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存推荐会员
     */
    @RequiresPermissions("app:recommentMember:add")
    @Log(title = "推荐会员", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(RecommentMember recommentMember)
    {
        return toAjax(recommentMemberService.insertRecommentMember(recommentMember));
    }

    /**
     * 修改推荐会员
     */
    @GetMapping("/edit/{recId}")
    public String edit(@PathVariable("recId") String recId, ModelMap mmap)
    {
        RecommentMember recommentMember = recommentMemberService.selectRecommentMemberById(recId);
        mmap.put("recommentMember", recommentMember);
        return prefix + "/edit";
    }

    /**
     * 修改保存推荐会员
     */
    @RequiresPermissions("app:recommentMember:edit")
    @Log(title = "推荐会员", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(RecommentMember recommentMember)
    {
        return toAjax(recommentMemberService.updateRecommentMember(recommentMember));
    }

    /**
     * 删除推荐会员
     */
    @RequiresPermissions("app:recommentMember:remove")
    @Log(title = "推荐会员", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(recommentMemberService.deleteRecommentMemberByIds(ids));
    }
}
