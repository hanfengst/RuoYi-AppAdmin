package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.TaskYetRec;
import com.ruoyi.app.service.ITaskYetRecService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 任务记录Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/taskYetRec")
public class TaskYetRecController extends BaseController
{
    private String prefix = "app/taskYetRec";

    @Autowired
    private ITaskYetRecService taskYetRecService;

    @RequiresPermissions("app:taskYetRec:view")
    @GetMapping()
    public String taskYetRec()
    {
        return prefix + "/taskYetRec";
    }

    /**
     * 查询任务记录列表
     */
    @RequiresPermissions("app:taskYetRec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TaskYetRec taskYetRec)
    {
        startPage();
        List<TaskYetRec> list = taskYetRecService.selectTaskYetRecList(taskYetRec);
        return getDataTable(list);
    }

    /**
     * 导出任务记录列表
     */
    @RequiresPermissions("app:taskYetRec:export")
    @Log(title = "任务记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TaskYetRec taskYetRec)
    {
        List<TaskYetRec> list = taskYetRecService.selectTaskYetRecList(taskYetRec);
        ExcelUtil<TaskYetRec> util = new ExcelUtil<TaskYetRec>(TaskYetRec.class);
        return util.exportExcel(list, "taskYetRec");
    }

    /**
     * 新增任务记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存任务记录
     */
    @RequiresPermissions("app:taskYetRec:add")
    @Log(title = "任务记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TaskYetRec taskYetRec)
    {
        return toAjax(taskYetRecService.insertTaskYetRec(taskYetRec));
    }

    /**
     * 修改任务记录
     */
    @GetMapping("/edit/{yetId}")
    public String edit(@PathVariable("yetId") String yetId, ModelMap mmap)
    {
        TaskYetRec taskYetRec = taskYetRecService.selectTaskYetRecById(yetId);
        mmap.put("taskYetRec", taskYetRec);
        return prefix + "/edit";
    }

    /**
     * 修改保存任务记录
     */
    @RequiresPermissions("app:taskYetRec:edit")
    @Log(title = "任务记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TaskYetRec taskYetRec)
    {
        return toAjax(taskYetRecService.updateTaskYetRec(taskYetRec));
    }

    /**
     * 删除任务记录
     */
    @RequiresPermissions("app:taskYetRec:remove")
    @Log(title = "任务记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(taskYetRecService.deleteTaskYetRecByIds(ids));
    }
}
