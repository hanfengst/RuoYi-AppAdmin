package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.CollectAct;
import com.ruoyi.app.service.ICollectActService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 收藏活动Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/collectAct")
public class CollectActController extends BaseController
{
    private String prefix = "app/collectAct";

    @Autowired
    private ICollectActService collectActService;

    @RequiresPermissions("app:collectAct:view")
    @GetMapping()
    public String collectAct()
    {
        return prefix + "/collectAct";
    }

    /**
     * 查询收藏活动列表
     */
    @RequiresPermissions("app:collectAct:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CollectAct collectAct)
    {
        startPage();
        List<CollectAct> list = collectActService.selectCollectActList(collectAct);
        return getDataTable(list);
    }

    /**
     * 导出收藏活动列表
     */
    @RequiresPermissions("app:collectAct:export")
    @Log(title = "收藏活动", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CollectAct collectAct)
    {
        List<CollectAct> list = collectActService.selectCollectActList(collectAct);
        ExcelUtil<CollectAct> util = new ExcelUtil<CollectAct>(CollectAct.class);
        return util.exportExcel(list, "collectAct");
    }

    /**
     * 新增收藏活动
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存收藏活动
     */
    @RequiresPermissions("app:collectAct:add")
    @Log(title = "收藏活动", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CollectAct collectAct)
    {
        return toAjax(collectActService.insertCollectAct(collectAct));
    }

    /**
     * 修改收藏活动
     */
    @GetMapping("/edit/{collectId}")
    public String edit(@PathVariable("collectId") Long collectId, ModelMap mmap)
    {
        CollectAct collectAct = collectActService.selectCollectActById(collectId);
        mmap.put("collectAct", collectAct);
        return prefix + "/edit";
    }

    /**
     * 修改保存收藏活动
     */
    @RequiresPermissions("app:collectAct:edit")
    @Log(title = "收藏活动", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CollectAct collectAct)
    {
        return toAjax(collectActService.updateCollectAct(collectAct));
    }

    /**
     * 删除收藏活动
     */
    @RequiresPermissions("app:collectAct:remove")
    @Log(title = "收藏活动", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(collectActService.deleteCollectActByIds(ids));
    }
}
