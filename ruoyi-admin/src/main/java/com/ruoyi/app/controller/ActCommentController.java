package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.ActComment;
import com.ruoyi.app.service.IActCommentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 活动评价Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/actComment")
public class ActCommentController extends BaseController
{
    private String prefix = "app/actComment";

    @Autowired
    private IActCommentService actCommentService;

    @RequiresPermissions("app:actComment:view")
    @GetMapping()
    public String actComment()
    {
        return prefix + "/actComment";
    }

    /**
     * 查询活动评价列表
     */
    @RequiresPermissions("app:actComment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ActComment actComment)
    {
        startPage();
        List<ActComment> list = actCommentService.selectActCommentList(actComment);
        return getDataTable(list);
    }

    /**
     * 导出活动评价列表
     */
    @RequiresPermissions("app:actComment:export")
    @Log(title = "活动评价", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ActComment actComment)
    {
        List<ActComment> list = actCommentService.selectActCommentList(actComment);
        ExcelUtil<ActComment> util = new ExcelUtil<ActComment>(ActComment.class);
        return util.exportExcel(list, "actComment");
    }

    /**
     * 新增活动评价
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存活动评价
     */
    @RequiresPermissions("app:actComment:add")
    @Log(title = "活动评价", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ActComment actComment)
    {
        return toAjax(actCommentService.insertActComment(actComment));
    }

    /**
     * 修改活动评价
     */
    @GetMapping("/edit/{actCommId}")
    public String edit(@PathVariable("actCommId") Long actCommId, ModelMap mmap)
    {
        ActComment actComment = actCommentService.selectActCommentById(actCommId);
        mmap.put("actComment", actComment);
        return prefix + "/edit";
    }

    /**
     * 修改保存活动评价
     */
    @RequiresPermissions("app:actComment:edit")
    @Log(title = "活动评价", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ActComment actComment)
    {
        return toAjax(actCommentService.updateActComment(actComment));
    }

    /**
     * 删除活动评价
     */
    @RequiresPermissions("app:actComment:remove")
    @Log(title = "活动评价", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(actCommentService.deleteActCommentByIds(ids));
    }
}
