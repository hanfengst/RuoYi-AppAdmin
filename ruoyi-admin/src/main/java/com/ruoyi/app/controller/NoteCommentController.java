package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.NoteComment;
import com.ruoyi.app.service.INoteCommentService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 游记评论Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/noteComment")
public class NoteCommentController extends BaseController
{
    private String prefix = "app/noteComment";

    @Autowired
    private INoteCommentService noteCommentService;

    @RequiresPermissions("app:noteComment:view")
    @GetMapping()
    public String noteComment()
    {
        return prefix + "/noteComment";
    }

    /**
     * 查询游记评论列表
     */
    @RequiresPermissions("app:noteComment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NoteComment noteComment)
    {
        startPage();
        List<NoteComment> list = noteCommentService.selectNoteCommentList(noteComment);
        return getDataTable(list);
    }

    /**
     * 导出游记评论列表
     */
    @RequiresPermissions("app:noteComment:export")
    @Log(title = "游记评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NoteComment noteComment)
    {
        List<NoteComment> list = noteCommentService.selectNoteCommentList(noteComment);
        ExcelUtil<NoteComment> util = new ExcelUtil<NoteComment>(NoteComment.class);
        return util.exportExcel(list, "noteComment");
    }

    /**
     * 新增游记评论
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存游记评论
     */
    @RequiresPermissions("app:noteComment:add")
    @Log(title = "游记评论", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NoteComment noteComment)
    {
        return toAjax(noteCommentService.insertNoteComment(noteComment));
    }

    /**
     * 修改游记评论
     */
    @GetMapping("/edit/{noteId}")
    public String edit(@PathVariable("noteId") String noteId, ModelMap mmap)
    {
        NoteComment noteComment = noteCommentService.selectNoteCommentById(noteId);
        mmap.put("noteComment", noteComment);
        return prefix + "/edit";
    }

    /**
     * 修改保存游记评论
     */
    @RequiresPermissions("app:noteComment:edit")
    @Log(title = "游记评论", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NoteComment noteComment)
    {
        return toAjax(noteCommentService.updateNoteComment(noteComment));
    }

    /**
     * 删除游记评论
     */
    @RequiresPermissions("app:noteComment:remove")
    @Log(title = "游记评论", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(noteCommentService.deleteNoteCommentByIds(ids));
    }
}
