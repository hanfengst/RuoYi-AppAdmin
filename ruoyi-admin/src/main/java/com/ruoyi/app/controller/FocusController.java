package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.Focus;
import com.ruoyi.app.service.IFocusService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 关注Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/focus")
public class FocusController extends BaseController
{
    private String prefix = "app/focus";

    @Autowired
    private IFocusService focusService;

    @RequiresPermissions("app:focus:view")
    @GetMapping()
    public String focus()
    {
        return prefix + "/focus";
    }

    /**
     * 查询关注列表
     */
    @RequiresPermissions("app:focus:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Focus focus)
    {
        startPage();
        List<Focus> list = focusService.selectFocusList(focus);
        return getDataTable(list);
    }

    /**
     * 导出关注列表
     */
    @RequiresPermissions("app:focus:export")
    @Log(title = "关注", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Focus focus)
    {
        List<Focus> list = focusService.selectFocusList(focus);
        ExcelUtil<Focus> util = new ExcelUtil<Focus>(Focus.class);
        return util.exportExcel(list, "focus");
    }

    /**
     * 新增关注
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存关注
     */
    @RequiresPermissions("app:focus:add")
    @Log(title = "关注", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Focus focus)
    {
        return toAjax(focusService.insertFocus(focus));
    }

    /**
     * 修改关注
     */
    @GetMapping("/edit/{focusId}")
    public String edit(@PathVariable("focusId") Long focusId, ModelMap mmap)
    {
        Focus focus = focusService.selectFocusById(focusId);
        mmap.put("focus", focus);
        return prefix + "/edit";
    }

    /**
     * 修改保存关注
     */
    @RequiresPermissions("app:focus:edit")
    @Log(title = "关注", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Focus focus)
    {
        return toAjax(focusService.updateFocus(focus));
    }

    /**
     * 删除关注
     */
    @RequiresPermissions("app:focus:remove")
    @Log(title = "关注", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(focusService.deleteFocusByIds(ids));
    }
}
