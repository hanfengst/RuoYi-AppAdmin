package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.FaceRec;
import com.ruoyi.app.service.IFaceRecService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 关注Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/faceRec")
public class FaceRecController extends BaseController
{
    private String prefix = "app/faceRec";

    @Autowired
    private IFaceRecService faceRecService;

    @RequiresPermissions("app:faceRec:view")
    @GetMapping()
    public String faceRec()
    {
        return prefix + "/faceRec";
    }

    /**
     * 查询关注列表
     */
    @RequiresPermissions("app:faceRec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(FaceRec faceRec)
    {
        startPage();
        List<FaceRec> list = faceRecService.selectFaceRecList(faceRec);
        return getDataTable(list);
    }

    /**
     * 导出关注列表
     */
    @RequiresPermissions("app:faceRec:export")
    @Log(title = "关注", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(FaceRec faceRec)
    {
        List<FaceRec> list = faceRecService.selectFaceRecList(faceRec);
        ExcelUtil<FaceRec> util = new ExcelUtil<FaceRec>(FaceRec.class);
        return util.exportExcel(list, "faceRec");
    }

    /**
     * 新增关注
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存关注
     */
    @RequiresPermissions("app:faceRec:add")
    @Log(title = "关注", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(FaceRec faceRec)
    {
        return toAjax(faceRecService.insertFaceRec(faceRec));
    }

    /**
     * 修改关注
     */
    @GetMapping("/edit/{faceId}")
    public String edit(@PathVariable("faceId") String faceId, ModelMap mmap)
    {
        FaceRec faceRec = faceRecService.selectFaceRecById(faceId);
        mmap.put("faceRec", faceRec);
        return prefix + "/edit";
    }

    /**
     * 修改保存关注
     */
    @RequiresPermissions("app:faceRec:edit")
    @Log(title = "关注", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(FaceRec faceRec)
    {
        return toAjax(faceRecService.updateFaceRec(faceRec));
    }

    /**
     * 删除关注
     */
    @RequiresPermissions("app:faceRec:remove")
    @Log(title = "关注", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(faceRecService.deleteFaceRecByIds(ids));
    }
}
