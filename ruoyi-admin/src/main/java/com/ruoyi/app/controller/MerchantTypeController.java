package com.ruoyi.app.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app.domain.MerchantType;
import com.ruoyi.app.service.IMerchantTypeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商家分类Controller
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Controller
@RequestMapping("/app/merchantType")
public class MerchantTypeController extends BaseController
{
    private String prefix = "app/merchantType";

    @Autowired
    private IMerchantTypeService merchantTypeService;

    @RequiresPermissions("app:merchantType:view")
    @GetMapping()
    public String merchantType()
    {
        return prefix + "/merchantType";
    }

    /**
     * 查询商家分类列表
     */
    @RequiresPermissions("app:merchantType:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MerchantType merchantType)
    {
        startPage();
        List<MerchantType> list = merchantTypeService.selectMerchantTypeList(merchantType);
        return getDataTable(list);
    }

    /**
     * 导出商家分类列表
     */
    @RequiresPermissions("app:merchantType:export")
    @Log(title = "商家分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MerchantType merchantType)
    {
        List<MerchantType> list = merchantTypeService.selectMerchantTypeList(merchantType);
        ExcelUtil<MerchantType> util = new ExcelUtil<MerchantType>(MerchantType.class);
        return util.exportExcel(list, "merchantType");
    }

    /**
     * 新增商家分类
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商家分类
     */
    @RequiresPermissions("app:merchantType:add")
    @Log(title = "商家分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MerchantType merchantType)
    {
        return toAjax(merchantTypeService.insertMerchantType(merchantType));
    }

    /**
     * 修改商家分类
     */
    @GetMapping("/edit/{merTypeId}")
    public String edit(@PathVariable("merTypeId") Long merTypeId, ModelMap mmap)
    {
        MerchantType merchantType = merchantTypeService.selectMerchantTypeById(merTypeId);
        mmap.put("merchantType", merchantType);
        return prefix + "/edit";
    }

    /**
     * 修改保存商家分类
     */
    @RequiresPermissions("app:merchantType:edit")
    @Log(title = "商家分类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MerchantType merchantType)
    {
        return toAjax(merchantTypeService.updateMerchantType(merchantType));
    }

    /**
     * 删除商家分类
     */
    @RequiresPermissions("app:merchantType:remove")
    @Log(title = "商家分类", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(merchantTypeService.deleteMerchantTypeByIds(ids));
    }
}
