package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 推荐活动对象 app_recomment_act
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class RecommentAct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String recId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 推荐活动 */
    @Excel(name = "推荐活动")
    private Long actIdRec;

    public void setRecId(String recId) 
    {
        this.recId = recId;
    }

    public String getRecId() 
    {
        return recId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setActIdRec(Long actIdRec) 
    {
        this.actIdRec = actIdRec;
    }

    public Long getActIdRec() 
    {
        return actIdRec;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("recId", getRecId())
            .append("memberId", getMemberId())
            .append("actIdRec", getActIdRec())
            .append("createTime", getCreateTime())
            .toString();
    }
}
