package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 兴趣标签对象 app_hobby_tag
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class HobbyTag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** tag_id */
    @Excel(name = "tag_id")
    private String tagId;

    /** 名称 */
    @Excel(name = "名称")
    private String tagName;

    /** 排序数小在前 */
    @Excel(name = "排序数小在前")
    private Long sortNum;

    public void setTagId(String tagId) 
    {
        this.tagId = tagId;
    }

    public String getTagId() 
    {
        return tagId;
    }
    public void setTagName(String tagName) 
    {
        this.tagName = tagName;
    }

    public String getTagName() 
    {
        return tagName;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tagId", getTagId())
            .append("tagName", getTagName())
            .append("sortNum", getSortNum())
            .append("createTime", getCreateTime())
            .toString();
    }
}
