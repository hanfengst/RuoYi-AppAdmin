package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 广告对象 app_advert
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class Advert extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ad_id */
    private Long adId;

    /** 广告位置1-首页；2-开屏广告 */
    @Excel(name = "广告位置1-首页；2-开屏广告")
    private Integer adPosition;

    /** 是否启动1-是，0-否 */
    @Excel(name = "是否启动1-是，0-否")
    private Integer canUse;

    /** 跳转类型0-聚会1-会员2-任务 */
    @Excel(name = "跳转类型0-聚会1-会员2-任务")
    private Integer linkType;

    /** 跳转数据 */
    @Excel(name = "跳转数据")
    private String linkData;

    /** 图片路径 */
    @Excel(name = "图片路径")
    private String imgSrc;

    /** 展示顺序 */
    @Excel(name = "展示顺序")
    private Long sortNum;

    public void setAdId(Long adId) 
    {
        this.adId = adId;
    }

    public Long getAdId() 
    {
        return adId;
    }
    public void setAdPosition(Integer adPosition) 
    {
        this.adPosition = adPosition;
    }

    public Integer getAdPosition() 
    {
        return adPosition;
    }
    public void setCanUse(Integer canUse) 
    {
        this.canUse = canUse;
    }

    public Integer getCanUse() 
    {
        return canUse;
    }
    public void setLinkType(Integer linkType) 
    {
        this.linkType = linkType;
    }

    public Integer getLinkType() 
    {
        return linkType;
    }
    public void setLinkData(String linkData) 
    {
        this.linkData = linkData;
    }

    public String getLinkData() 
    {
        return linkData;
    }
    public void setImgSrc(String imgSrc) 
    {
        this.imgSrc = imgSrc;
    }

    public String getImgSrc() 
    {
        return imgSrc;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("adId", getAdId())
            .append("adPosition", getAdPosition())
            .append("canUse", getCanUse())
            .append("linkType", getLinkType())
            .append("linkData", getLinkData())
            .append("createTime", getCreateTime())
            .append("imgSrc", getImgSrc())
            .append("remark", getRemark())
            .append("sortNum", getSortNum())
            .toString();
    }
}
