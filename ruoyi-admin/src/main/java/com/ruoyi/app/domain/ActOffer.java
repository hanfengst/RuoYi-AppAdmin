package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 活动邀约对象 app_act_offer
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class ActOffer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 收藏ID */
    private String offerId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 被邀会员 */
    @Excel(name = "被邀会员")
    private String memberIdTo;

    /** 活动ID */
    @Excel(name = "活动ID")
    private String actId;

    /** 状态，0-未查看，1-已查看，2-未同意，3-同意 */
    @Excel(name = "状态，0-未查看，1-已查看，2-未同意，3-同意")
    private Integer state;

    public void setOfferId(String offerId) 
    {
        this.offerId = offerId;
    }

    public String getOfferId() 
    {
        return offerId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdTo(String memberIdTo) 
    {
        this.memberIdTo = memberIdTo;
    }

    public String getMemberIdTo() 
    {
        return memberIdTo;
    }
    public void setActId(String actId) 
    {
        this.actId = actId;
    }

    public String getActId() 
    {
        return actId;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("offerId", getOfferId())
            .append("memberId", getMemberId())
            .append("memberIdTo", getMemberIdTo())
            .append("actId", getActId())
            .append("createTime", getCreateTime())
            .append("state", getState())
            .toString();
    }
}
