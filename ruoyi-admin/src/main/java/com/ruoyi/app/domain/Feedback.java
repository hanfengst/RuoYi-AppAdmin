package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 意见反馈对象 app_feedback
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class Feedback extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String feedId;

    /** 联系人 */
    @Excel(name = "联系人")
    private String linkman;

    /** 建议 */
    @Excel(name = "建议")
    private String suggest;

    /** 应用评分(0-5) */
    @Excel(name = "应用评分(0-5)")
    private Integer appScore;

    /** 反馈图片 */
    @Excel(name = "反馈图片")
    private String feedImg;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    public void setFeedId(String feedId) 
    {
        this.feedId = feedId;
    }

    public String getFeedId() 
    {
        return feedId;
    }
    public void setLinkman(String linkman) 
    {
        this.linkman = linkman;
    }

    public String getLinkman() 
    {
        return linkman;
    }
    public void setSuggest(String suggest) 
    {
        this.suggest = suggest;
    }

    public String getSuggest() 
    {
        return suggest;
    }
    public void setAppScore(Integer appScore) 
    {
        this.appScore = appScore;
    }

    public Integer getAppScore() 
    {
        return appScore;
    }
    public void setFeedImg(String feedImg) 
    {
        this.feedImg = feedImg;
    }

    public String getFeedImg() 
    {
        return feedImg;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("feedId", getFeedId())
            .append("linkman", getLinkman())
            .append("suggest", getSuggest())
            .append("appScore", getAppScore())
            .append("feedImg", getFeedImg())
            .append("memberId", getMemberId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
