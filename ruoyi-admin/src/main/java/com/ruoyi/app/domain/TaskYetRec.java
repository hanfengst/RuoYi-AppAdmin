package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 任务记录对象 app_task_yet_rec
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class TaskYetRec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String yetId;

    /** 任务编号 */
    @Excel(name = "任务编号")
    private String taskId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 完成状态0-未完成，1-已完成 */
    @Excel(name = "完成状态0-未完成，1-已完成")
    private Integer state;

    public void setYetId(String yetId) 
    {
        this.yetId = yetId;
    }

    public String getYetId() 
    {
        return yetId;
    }
    public void setTaskId(String taskId) 
    {
        this.taskId = taskId;
    }

    public String getTaskId() 
    {
        return taskId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("yetId", getYetId())
            .append("taskId", getTaskId())
            .append("memberId", getMemberId())
            .append("createTime", getCreateTime())
            .append("state", getState())
            .toString();
    }
}
