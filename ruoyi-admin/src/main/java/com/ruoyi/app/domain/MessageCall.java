package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 招呼对象 app_message_call
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class MessageCall extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String callId;

    /** 发送者 */
    @Excel(name = "发送者")
    private String memberId;

    /** 接收方 */
    @Excel(name = "接收方")
    private String memberIdTo;

    /** 发送时间 */
    @Excel(name = "发送时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sendTime;

    /** 状态0-未查看，1-已查看，2-已答应 */
    @Excel(name = "状态0-未查看，1-已查看，2-已答应")
    private Integer state;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    public void setCallId(String callId) 
    {
        this.callId = callId;
    }

    public String getCallId() 
    {
        return callId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setMemberIdTo(String memberIdTo) 
    {
        this.memberIdTo = memberIdTo;
    }

    public String getMemberIdTo() 
    {
        return memberIdTo;
    }
    public void setSendTime(Date sendTime) 
    {
        this.sendTime = sendTime;
    }

    public Date getSendTime() 
    {
        return sendTime;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("callId", getCallId())
            .append("memberId", getMemberId())
            .append("memberIdTo", getMemberIdTo())
            .append("sendTime", getSendTime())
            .append("state", getState())
            .append("content", getContent())
            .toString();
    }
}
