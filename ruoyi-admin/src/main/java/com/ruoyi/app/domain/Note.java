package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 游记对象 app_note
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class Note extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ad_id */
    private String noteId;

    /** 游记类型 */
    @Excel(name = "游记类型")
    private Long noteTypeId;

    /** 标题 */
    @Excel(name = "标题")
    private String noteTitle;

    /** 话题列表 */
    @Excel(name = "话题列表")
    private String topicTitle;

    /** 封面 */
    @Excel(name = "封面")
    private String coverImg;

    /** 照片集 */
    @Excel(name = "照片集")
    private String adImg;

    /** 参考费用 */
    @Excel(name = "参考费用")
    private Double price;

    /** 地点 */
    @Excel(name = "地点")
    private String place;

    /** 描述 */
    @Excel(name = "描述")
    private String noteDesc;

    /** 浏览量 */
    @Excel(name = "浏览量")
    private Long pv;

    /** 想去数量 */
    @Excel(name = "想去数量")
    private Long wantgoNum;

    /** 状态0-暂存，1-提交，2-审核通过，3-审核未通过 */
    @Excel(name = "状态0-暂存，1-提交，2-审核通过，3-审核未通过")
    private Integer state;

    /** 发布会员ID */
    @Excel(name = "发布会员ID")
    private String publishMemberId;

    /** 分享次数 */
    @Excel(name = "分享次数")
    private Long shareNumber;

    /** 纬度(发布时获取坐标) */
    @Excel(name = "纬度(发布时获取坐标)")
    private Double lat;

    /** 经度 */
    @Excel(name = "经度")
    private Double lng;

    /** 展示顺序 */
    @Excel(name = "展示顺序")
    private Long sortNum;

    /** 城市编号 */
    @Excel(name = "城市编号")
    private String cityCode;

    /** delta描述对象 */
    @Excel(name = "delta描述对象")
    private String deltaObj;

    /** 推荐权重1-100 */
    @Excel(name = "推荐权重1-100")
    private Long showWeight;

    public void setNoteId(String noteId) 
    {
        this.noteId = noteId;
    }

    public String getNoteId() 
    {
        return noteId;
    }
    public void setNoteTypeId(Long noteTypeId) 
    {
        this.noteTypeId = noteTypeId;
    }

    public Long getNoteTypeId() 
    {
        return noteTypeId;
    }
    public void setNoteTitle(String noteTitle) 
    {
        this.noteTitle = noteTitle;
    }

    public String getNoteTitle() 
    {
        return noteTitle;
    }
    public void setTopicTitle(String topicTitle) 
    {
        this.topicTitle = topicTitle;
    }

    public String getTopicTitle() 
    {
        return topicTitle;
    }
    public void setCoverImg(String coverImg) 
    {
        this.coverImg = coverImg;
    }

    public String getCoverImg() 
    {
        return coverImg;
    }
    public void setAdImg(String adImg) 
    {
        this.adImg = adImg;
    }

    public String getAdImg() 
    {
        return adImg;
    }
    public void setPrice(Double price) 
    {
        this.price = price;
    }

    public Double getPrice() 
    {
        return price;
    }
    public void setPlace(String place) 
    {
        this.place = place;
    }

    public String getPlace() 
    {
        return place;
    }
    public void setNoteDesc(String noteDesc) 
    {
        this.noteDesc = noteDesc;
    }

    public String getNoteDesc() 
    {
        return noteDesc;
    }
    public void setPv(Long pv) 
    {
        this.pv = pv;
    }

    public Long getPv() 
    {
        return pv;
    }
    public void setWantgoNum(Long wantgoNum) 
    {
        this.wantgoNum = wantgoNum;
    }

    public Long getWantgoNum() 
    {
        return wantgoNum;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setPublishMemberId(String publishMemberId) 
    {
        this.publishMemberId = publishMemberId;
    }

    public String getPublishMemberId() 
    {
        return publishMemberId;
    }
    public void setShareNumber(Long shareNumber) 
    {
        this.shareNumber = shareNumber;
    }

    public Long getShareNumber() 
    {
        return shareNumber;
    }
    public void setLat(Double lat) 
    {
        this.lat = lat;
    }

    public Double getLat() 
    {
        return lat;
    }
    public void setLng(Double lng) 
    {
        this.lng = lng;
    }

    public Double getLng() 
    {
        return lng;
    }
    public void setSortNum(Long sortNum) 
    {
        this.sortNum = sortNum;
    }

    public Long getSortNum() 
    {
        return sortNum;
    }
    public void setCityCode(String cityCode) 
    {
        this.cityCode = cityCode;
    }

    public String getCityCode() 
    {
        return cityCode;
    }
    public void setDeltaObj(String deltaObj) 
    {
        this.deltaObj = deltaObj;
    }

    public String getDeltaObj() 
    {
        return deltaObj;
    }
    public void setShowWeight(Long showWeight) 
    {
        this.showWeight = showWeight;
    }

    public Long getShowWeight() 
    {
        return showWeight;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("noteId", getNoteId())
            .append("noteTypeId", getNoteTypeId())
            .append("noteTitle", getNoteTitle())
            .append("topicTitle", getTopicTitle())
            .append("coverImg", getCoverImg())
            .append("adImg", getAdImg())
            .append("price", getPrice())
            .append("place", getPlace())
            .append("noteDesc", getNoteDesc())
            .append("pv", getPv())
            .append("wantgoNum", getWantgoNum())
            .append("state", getState())
            .append("publishMemberId", getPublishMemberId())
            .append("shareNumber", getShareNumber())
            .append("lat", getLat())
            .append("lng", getLng())
            .append("sortNum", getSortNum())
            .append("createTime", getCreateTime())
            .append("cityCode", getCityCode())
            .append("deltaObj", getDeltaObj())
            .append("showWeight", getShowWeight())
            .toString();
    }
}
