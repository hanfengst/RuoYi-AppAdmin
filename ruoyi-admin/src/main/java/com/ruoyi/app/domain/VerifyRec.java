package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 验证码记录对象 app_verify_rec
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class VerifyRec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String verifyId;

    /** 验证码 */
    @Excel(name = "验证码")
    private String verifyCode;

    /** 类型0-邮箱，1--手机 */
    @Excel(name = "类型0-邮箱，1--手机")
    private Integer verifyType;

    /** 场景0-注册，1-忘记密码 */
    @Excel(name = "场景0-注册，1-忘记密码")
    private Integer verifyScene;

    /** 验证的账号 */
    @Excel(name = "验证的账号")
    private String verifyAccount;

    public void setVerifyId(String verifyId) 
    {
        this.verifyId = verifyId;
    }

    public String getVerifyId() 
    {
        return verifyId;
    }
    public void setVerifyCode(String verifyCode) 
    {
        this.verifyCode = verifyCode;
    }

    public String getVerifyCode() 
    {
        return verifyCode;
    }
    public void setVerifyType(Integer verifyType) 
    {
        this.verifyType = verifyType;
    }

    public Integer getVerifyType() 
    {
        return verifyType;
    }
    public void setVerifyScene(Integer verifyScene) 
    {
        this.verifyScene = verifyScene;
    }

    public Integer getVerifyScene() 
    {
        return verifyScene;
    }
    public void setVerifyAccount(String verifyAccount) 
    {
        this.verifyAccount = verifyAccount;
    }

    public String getVerifyAccount() 
    {
        return verifyAccount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("verifyId", getVerifyId())
            .append("verifyCode", getVerifyCode())
            .append("verifyType", getVerifyType())
            .append("verifyScene", getVerifyScene())
            .append("createTime", getCreateTime())
            .append("verifyAccount", getVerifyAccount())
            .toString();
    }
}
