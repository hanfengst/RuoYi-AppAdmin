package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 趣币交易对象 app_coin_deal
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class CoinDeal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** deal_id */
    private String dealId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 交易类型，1-新增；0-减少 */
    @Excel(name = "交易类型，1-新增；0-减少")
    private Integer dealType;

    /** 趣币数量 */
    @Excel(name = "趣币数量")
    private Long coinNum;

    /** 核对状态1-已核实，2-未核实，0-待核实 */
    @Excel(name = "核对状态1-已核实，2-未核实，0-待核实")
    private Long state;

    public void setDealId(String dealId) 
    {
        this.dealId = dealId;
    }

    public String getDealId() 
    {
        return dealId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setDealType(Integer dealType) 
    {
        this.dealType = dealType;
    }

    public Integer getDealType() 
    {
        return dealType;
    }
    public void setCoinNum(Long coinNum) 
    {
        this.coinNum = coinNum;
    }

    public Long getCoinNum() 
    {
        return coinNum;
    }
    public void setState(Long state) 
    {
        this.state = state;
    }

    public Long getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dealId", getDealId())
            .append("memberId", getMemberId())
            .append("dealType", getDealType())
            .append("createTime", getCreateTime())
            .append("coinNum", getCoinNum())
            .append("remark", getRemark())
            .append("state", getState())
            .toString();
    }
}
