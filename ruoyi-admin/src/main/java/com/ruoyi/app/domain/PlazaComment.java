package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 广场评论对象 app_plaza_comment
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class PlazaComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String plazaCommId;

    /** 内容ID */
    @Excel(name = "内容ID")
    private String contentId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 状态0-审核不通过，1-审核通过 */
    @Excel(name = "状态0-审核不通过，1-审核通过")
    private Integer state;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    public void setPlazaCommId(String plazaCommId) 
    {
        this.plazaCommId = plazaCommId;
    }

    public String getPlazaCommId() 
    {
        return plazaCommId;
    }
    public void setContentId(String contentId) 
    {
        this.contentId = contentId;
    }

    public String getContentId() 
    {
        return contentId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setState(Integer state) 
    {
        this.state = state;
    }

    public Integer getState() 
    {
        return state;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("plazaCommId", getPlazaCommId())
            .append("contentId", getContentId())
            .append("memberId", getMemberId())
            .append("state", getState())
            .append("createTime", getCreateTime())
            .append("content", getContent())
            .toString();
    }
}
