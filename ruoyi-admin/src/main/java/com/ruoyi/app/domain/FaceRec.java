package com.ruoyi.app.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 关注对象 app_face_rec
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public class FaceRec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    private String faceId;

    /** 会员ID */
    @Excel(name = "会员ID")
    private String memberId;

    /** 颜值图片路径 */
    @Excel(name = "颜值图片路径")
    private String facePath;

    /** 颜值数据 */
    @Excel(name = "颜值数据")
    private String faceValue;

    /** 颜值分数 */
    @Excel(name = "颜值分数")
    private Double faceScore;

    public void setFaceId(String faceId) 
    {
        this.faceId = faceId;
    }

    public String getFaceId() 
    {
        return faceId;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setFacePath(String facePath) 
    {
        this.facePath = facePath;
    }

    public String getFacePath() 
    {
        return facePath;
    }
    public void setFaceValue(String faceValue) 
    {
        this.faceValue = faceValue;
    }

    public String getFaceValue() 
    {
        return faceValue;
    }
    public void setFaceScore(Double faceScore) 
    {
        this.faceScore = faceScore;
    }

    public Double getFaceScore() 
    {
        return faceScore;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("faceId", getFaceId())
            .append("memberId", getMemberId())
            .append("facePath", getFacePath())
            .append("faceValue", getFaceValue())
            .append("faceScore", getFaceScore())
            .append("createTime", getCreateTime())
            .toString();
    }
}
