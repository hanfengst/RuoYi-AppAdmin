package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.RecommentMemberMapper;
import com.ruoyi.app.domain.RecommentMember;
import com.ruoyi.app.service.IRecommentMemberService;
import com.ruoyi.common.core.text.Convert;

/**
 * 推荐会员Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class RecommentMemberServiceImpl implements IRecommentMemberService 
{
    @Autowired
    private RecommentMemberMapper recommentMemberMapper;

    /**
     * 查询推荐会员
     * 
     * @param recId 推荐会员ID
     * @return 推荐会员
     */
    @Override
    public RecommentMember selectRecommentMemberById(String recId)
    {
        return recommentMemberMapper.selectRecommentMemberById(recId);
    }

    /**
     * 查询推荐会员列表
     * 
     * @param recommentMember 推荐会员
     * @return 推荐会员
     */
    @Override
    public List<RecommentMember> selectRecommentMemberList(RecommentMember recommentMember)
    {
        return recommentMemberMapper.selectRecommentMemberList(recommentMember);
    }

    /**
     * 新增推荐会员
     * 
     * @param recommentMember 推荐会员
     * @return 结果
     */
    @Override
    public int insertRecommentMember(RecommentMember recommentMember)
    {
        recommentMember.setCreateTime(DateUtils.getNowDate());
        return recommentMemberMapper.insertRecommentMember(recommentMember);
    }

    /**
     * 修改推荐会员
     * 
     * @param recommentMember 推荐会员
     * @return 结果
     */
    @Override
    public int updateRecommentMember(RecommentMember recommentMember)
    {
        return recommentMemberMapper.updateRecommentMember(recommentMember);
    }

    /**
     * 删除推荐会员对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecommentMemberByIds(String ids)
    {
        return recommentMemberMapper.deleteRecommentMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除推荐会员信息
     * 
     * @param recId 推荐会员ID
     * @return 结果
     */
    @Override
    public int deleteRecommentMemberById(String recId)
    {
        return recommentMemberMapper.deleteRecommentMemberById(recId);
    }
}
