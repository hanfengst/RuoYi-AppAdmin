package com.ruoyi.app.service;

import com.ruoyi.app.domain.PosterPsd;
import java.util.List;

/**
 * 举报Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IPosterPsdService 
{
    /**
     * 查询举报
     * 
     * @param psdId 举报ID
     * @return 举报
     */
    public PosterPsd selectPosterPsdById(Long psdId);

    /**
     * 查询举报列表
     * 
     * @param posterPsd 举报
     * @return 举报集合
     */
    public List<PosterPsd> selectPosterPsdList(PosterPsd posterPsd);

    /**
     * 新增举报
     * 
     * @param posterPsd 举报
     * @return 结果
     */
    public int insertPosterPsd(PosterPsd posterPsd);

    /**
     * 修改举报
     * 
     * @param posterPsd 举报
     * @return 结果
     */
    public int updatePosterPsd(PosterPsd posterPsd);

    /**
     * 批量删除举报
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePosterPsdByIds(String ids);

    /**
     * 删除举报信息
     * 
     * @param psdId 举报ID
     * @return 结果
     */
    public int deletePosterPsdById(Long psdId);
}
