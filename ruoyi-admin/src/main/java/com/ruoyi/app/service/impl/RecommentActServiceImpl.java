package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.RecommentActMapper;
import com.ruoyi.app.domain.RecommentAct;
import com.ruoyi.app.service.IRecommentActService;
import com.ruoyi.common.core.text.Convert;

/**
 * 推荐活动Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class RecommentActServiceImpl implements IRecommentActService 
{
    @Autowired
    private RecommentActMapper recommentActMapper;

    /**
     * 查询推荐活动
     * 
     * @param recId 推荐活动ID
     * @return 推荐活动
     */
    @Override
    public RecommentAct selectRecommentActById(String recId)
    {
        return recommentActMapper.selectRecommentActById(recId);
    }

    /**
     * 查询推荐活动列表
     * 
     * @param recommentAct 推荐活动
     * @return 推荐活动
     */
    @Override
    public List<RecommentAct> selectRecommentActList(RecommentAct recommentAct)
    {
        return recommentActMapper.selectRecommentActList(recommentAct);
    }

    /**
     * 新增推荐活动
     * 
     * @param recommentAct 推荐活动
     * @return 结果
     */
    @Override
    public int insertRecommentAct(RecommentAct recommentAct)
    {
        recommentAct.setCreateTime(DateUtils.getNowDate());
        return recommentActMapper.insertRecommentAct(recommentAct);
    }

    /**
     * 修改推荐活动
     * 
     * @param recommentAct 推荐活动
     * @return 结果
     */
    @Override
    public int updateRecommentAct(RecommentAct recommentAct)
    {
        return recommentActMapper.updateRecommentAct(recommentAct);
    }

    /**
     * 删除推荐活动对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRecommentActByIds(String ids)
    {
        return recommentActMapper.deleteRecommentActByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除推荐活动信息
     * 
     * @param recId 推荐活动ID
     * @return 结果
     */
    @Override
    public int deleteRecommentActById(String recId)
    {
        return recommentActMapper.deleteRecommentActById(recId);
    }
}
