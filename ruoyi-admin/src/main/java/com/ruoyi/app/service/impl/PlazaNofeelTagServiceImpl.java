package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.PlazaNofeelTagMapper;
import com.ruoyi.app.domain.PlazaNofeelTag;
import com.ruoyi.app.service.IPlazaNofeelTagService;
import com.ruoyi.common.core.text.Convert;

/**
 * 广场不感兴趣Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class PlazaNofeelTagServiceImpl implements IPlazaNofeelTagService 
{
    @Autowired
    private PlazaNofeelTagMapper plazaNofeelTagMapper;

    /**
     * 查询广场不感兴趣
     * 
     * @param nofeelId 广场不感兴趣ID
     * @return 广场不感兴趣
     */
    @Override
    public PlazaNofeelTag selectPlazaNofeelTagById(String nofeelId)
    {
        return plazaNofeelTagMapper.selectPlazaNofeelTagById(nofeelId);
    }

    /**
     * 查询广场不感兴趣列表
     * 
     * @param plazaNofeelTag 广场不感兴趣
     * @return 广场不感兴趣
     */
    @Override
    public List<PlazaNofeelTag> selectPlazaNofeelTagList(PlazaNofeelTag plazaNofeelTag)
    {
        return plazaNofeelTagMapper.selectPlazaNofeelTagList(plazaNofeelTag);
    }

    /**
     * 新增广场不感兴趣
     * 
     * @param plazaNofeelTag 广场不感兴趣
     * @return 结果
     */
    @Override
    public int insertPlazaNofeelTag(PlazaNofeelTag plazaNofeelTag)
    {
        plazaNofeelTag.setCreateTime(DateUtils.getNowDate());
        return plazaNofeelTagMapper.insertPlazaNofeelTag(plazaNofeelTag);
    }

    /**
     * 修改广场不感兴趣
     * 
     * @param plazaNofeelTag 广场不感兴趣
     * @return 结果
     */
    @Override
    public int updatePlazaNofeelTag(PlazaNofeelTag plazaNofeelTag)
    {
        return plazaNofeelTagMapper.updatePlazaNofeelTag(plazaNofeelTag);
    }

    /**
     * 删除广场不感兴趣对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePlazaNofeelTagByIds(String ids)
    {
        return plazaNofeelTagMapper.deletePlazaNofeelTagByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除广场不感兴趣信息
     * 
     * @param nofeelId 广场不感兴趣ID
     * @return 结果
     */
    @Override
    public int deletePlazaNofeelTagById(String nofeelId)
    {
        return plazaNofeelTagMapper.deletePlazaNofeelTagById(nofeelId);
    }
}
