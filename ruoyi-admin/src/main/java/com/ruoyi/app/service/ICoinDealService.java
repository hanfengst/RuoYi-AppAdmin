package com.ruoyi.app.service;

import com.ruoyi.app.domain.CoinDeal;
import java.util.List;

/**
 * 趣币交易Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface ICoinDealService 
{
    /**
     * 查询趣币交易
     * 
     * @param dealId 趣币交易ID
     * @return 趣币交易
     */
    public CoinDeal selectCoinDealById(String dealId);

    /**
     * 查询趣币交易列表
     * 
     * @param coinDeal 趣币交易
     * @return 趣币交易集合
     */
    public List<CoinDeal> selectCoinDealList(CoinDeal coinDeal);

    /**
     * 新增趣币交易
     * 
     * @param coinDeal 趣币交易
     * @return 结果
     */
    public int insertCoinDeal(CoinDeal coinDeal);

    /**
     * 修改趣币交易
     * 
     * @param coinDeal 趣币交易
     * @return 结果
     */
    public int updateCoinDeal(CoinDeal coinDeal);

    /**
     * 批量删除趣币交易
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCoinDealByIds(String ids);

    /**
     * 删除趣币交易信息
     * 
     * @param dealId 趣币交易ID
     * @return 结果
     */
    public int deleteCoinDealById(String dealId);
}
