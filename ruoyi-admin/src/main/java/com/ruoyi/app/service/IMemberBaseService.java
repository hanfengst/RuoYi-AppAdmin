package com.ruoyi.app.service;

import com.ruoyi.app.domain.MemberBase;
import java.util.List;

/**
 * 会员基础Service接口
 * 
 * @author bigStream
 * @date 2020-08-01
 */
public interface IMemberBaseService 
{
    /**
     * 查询会员基础
     * 
     * @param memberId 会员基础ID
     * @return 会员基础
     */
    public MemberBase selectMemberBaseById(String memberId);

    /**
     * 查询会员基础列表
     * 
     * @param memberBase 会员基础
     * @return 会员基础集合
     */
    public List<MemberBase> selectMemberBaseList(MemberBase memberBase);

    /**
     * 新增会员基础
     * 
     * @param memberBase 会员基础
     * @return 结果
     */
    public int insertMemberBase(MemberBase memberBase);

    /**
     * 修改会员基础
     * 
     * @param memberBase 会员基础
     * @return 结果
     */
    public int updateMemberBase(MemberBase memberBase);

    /**
     * 批量删除会员基础
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberBaseByIds(String ids);

    public int updateHeadAuditNOByIds(String ids);

    /**
     * 删除会员基础信息
     * 
     * @param memberId 会员基础ID
     * @return 结果
     */
    public int deleteMemberBaseById(String memberId);
}
