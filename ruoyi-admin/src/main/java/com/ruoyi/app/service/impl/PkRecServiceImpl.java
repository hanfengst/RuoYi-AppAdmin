package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.PkRecMapper;
import com.ruoyi.app.domain.PkRec;
import com.ruoyi.app.service.IPkRecService;
import com.ruoyi.common.core.text.Convert;

/**
 * pk比赛Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class PkRecServiceImpl implements IPkRecService 
{
    @Autowired
    private PkRecMapper pkRecMapper;

    /**
     * 查询pk比赛
     * 
     * @param pkId pk比赛ID
     * @return pk比赛
     */
    @Override
    public PkRec selectPkRecById(String pkId)
    {
        return pkRecMapper.selectPkRecById(pkId);
    }

    /**
     * 查询pk比赛列表
     * 
     * @param pkRec pk比赛
     * @return pk比赛
     */
    @Override
    public List<PkRec> selectPkRecList(PkRec pkRec)
    {
        return pkRecMapper.selectPkRecList(pkRec);
    }

    /**
     * 新增pk比赛
     * 
     * @param pkRec pk比赛
     * @return 结果
     */
    @Override
    public int insertPkRec(PkRec pkRec)
    {
        pkRec.setCreateTime(DateUtils.getNowDate());
        return pkRecMapper.insertPkRec(pkRec);
    }

    /**
     * 修改pk比赛
     * 
     * @param pkRec pk比赛
     * @return 结果
     */
    @Override
    public int updatePkRec(PkRec pkRec)
    {
        return pkRecMapper.updatePkRec(pkRec);
    }

    /**
     * 删除pk比赛对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePkRecByIds(String ids)
    {
        return pkRecMapper.deletePkRecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除pk比赛信息
     * 
     * @param pkId pk比赛ID
     * @return 结果
     */
    @Override
    public int deletePkRecById(String pkId)
    {
        return pkRecMapper.deletePkRecById(pkId);
    }
}
