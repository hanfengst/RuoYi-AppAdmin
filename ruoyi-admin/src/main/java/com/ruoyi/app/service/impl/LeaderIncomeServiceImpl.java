package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.LeaderIncomeMapper;
import com.ruoyi.app.domain.LeaderIncome;
import com.ruoyi.app.service.ILeaderIncomeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 领队收入Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class LeaderIncomeServiceImpl implements ILeaderIncomeService 
{
    @Autowired
    private LeaderIncomeMapper leaderIncomeMapper;

    /**
     * 查询领队收入
     * 
     * @param leaderIncomeId 领队收入ID
     * @return 领队收入
     */
    @Override
    public LeaderIncome selectLeaderIncomeById(String leaderIncomeId)
    {
        return leaderIncomeMapper.selectLeaderIncomeById(leaderIncomeId);
    }

    /**
     * 查询领队收入列表
     * 
     * @param leaderIncome 领队收入
     * @return 领队收入
     */
    @Override
    public List<LeaderIncome> selectLeaderIncomeList(LeaderIncome leaderIncome)
    {
        return leaderIncomeMapper.selectLeaderIncomeList(leaderIncome);
    }

    /**
     * 新增领队收入
     * 
     * @param leaderIncome 领队收入
     * @return 结果
     */
    @Override
    public int insertLeaderIncome(LeaderIncome leaderIncome)
    {
        leaderIncome.setCreateTime(DateUtils.getNowDate());
        return leaderIncomeMapper.insertLeaderIncome(leaderIncome);
    }

    /**
     * 修改领队收入
     * 
     * @param leaderIncome 领队收入
     * @return 结果
     */
    @Override
    public int updateLeaderIncome(LeaderIncome leaderIncome)
    {
        return leaderIncomeMapper.updateLeaderIncome(leaderIncome);
    }

    /**
     * 删除领队收入对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteLeaderIncomeByIds(String ids)
    {
        return leaderIncomeMapper.deleteLeaderIncomeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除领队收入信息
     * 
     * @param leaderIncomeId 领队收入ID
     * @return 结果
     */
    @Override
    public int deleteLeaderIncomeById(String leaderIncomeId)
    {
        return leaderIncomeMapper.deleteLeaderIncomeById(leaderIncomeId);
    }
}
