package com.ruoyi.app.service;

import com.ruoyi.app.domain.TaskRule;
import java.util.List;

/**
 * 任务定义Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface ITaskRuleService 
{
    /**
     * 查询任务定义
     * 
     * @param taskId 任务定义ID
     * @return 任务定义
     */
    public TaskRule selectTaskRuleById(String taskId);

    /**
     * 查询任务定义列表
     * 
     * @param taskRule 任务定义
     * @return 任务定义集合
     */
    public List<TaskRule> selectTaskRuleList(TaskRule taskRule);

    /**
     * 新增任务定义
     * 
     * @param taskRule 任务定义
     * @return 结果
     */
    public int insertTaskRule(TaskRule taskRule);

    /**
     * 修改任务定义
     * 
     * @param taskRule 任务定义
     * @return 结果
     */
    public int updateTaskRule(TaskRule taskRule);

    /**
     * 批量删除任务定义
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTaskRuleByIds(String ids);

    /**
     * 删除任务定义信息
     * 
     * @param taskId 任务定义ID
     * @return 结果
     */
    public int deleteTaskRuleById(String taskId);
}
