package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.NoteMapper;
import com.ruoyi.app.domain.Note;
import com.ruoyi.app.service.INoteService;
import com.ruoyi.common.core.text.Convert;

/**
 * 游记Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class NoteServiceImpl implements INoteService 
{
    @Autowired
    private NoteMapper noteMapper;

    /**
     * 查询游记
     * 
     * @param noteId 游记ID
     * @return 游记
     */
    @Override
    public Note selectNoteById(String noteId)
    {
        return noteMapper.selectNoteById(noteId);
    }

    /**
     * 查询游记列表
     * 
     * @param note 游记
     * @return 游记
     */
    @Override
    public List<Note> selectNoteList(Note note)
    {
        return noteMapper.selectNoteList(note);
    }

    /**
     * 新增游记
     * 
     * @param note 游记
     * @return 结果
     */
    @Override
    public int insertNote(Note note)
    {
        note.setCreateTime(DateUtils.getNowDate());
        return noteMapper.insertNote(note);
    }

    /**
     * 修改游记
     * 
     * @param note 游记
     * @return 结果
     */
    @Override
    public int updateNote(Note note)
    {
        return noteMapper.updateNote(note);
    }

    /**
     * 删除游记对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNoteByIds(String ids)
    {
        return noteMapper.deleteNoteByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除游记信息
     * 
     * @param noteId 游记ID
     * @return 结果
     */
    @Override
    public int deleteNoteById(String noteId)
    {
        return noteMapper.deleteNoteById(noteId);
    }
}
