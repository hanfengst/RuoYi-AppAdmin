package com.ruoyi.app.service;

import com.ruoyi.app.domain.ActMember;
import java.util.List;

/**
 * 报名记录Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IActMemberService 
{
    /**
     * 查询报名记录
     * 
     * @param acmId 报名记录ID
     * @return 报名记录
     */
    public ActMember selectActMemberById(String acmId);

    /**
     * 查询报名记录列表
     * 
     * @param actMember 报名记录
     * @return 报名记录集合
     */
    public List<ActMember> selectActMemberList(ActMember actMember);

    /**
     * 新增报名记录
     * 
     * @param actMember 报名记录
     * @return 结果
     */
    public int insertActMember(ActMember actMember);

    /**
     * 修改报名记录
     * 
     * @param actMember 报名记录
     * @return 结果
     */
    public int updateActMember(ActMember actMember);

    /**
     * 批量删除报名记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActMemberByIds(String ids);

    /**
     * 删除报名记录信息
     * 
     * @param acmId 报名记录ID
     * @return 结果
     */
    public int deleteActMemberById(String acmId);
}
