package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.NoteTypeMapper;
import com.ruoyi.app.domain.NoteType;
import com.ruoyi.app.service.INoteTypeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 游记类型Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class NoteTypeServiceImpl implements INoteTypeService 
{
    @Autowired
    private NoteTypeMapper noteTypeMapper;

    /**
     * 查询游记类型
     * 
     * @param noteTypeId 游记类型ID
     * @return 游记类型
     */
    @Override
    public NoteType selectNoteTypeById(Long noteTypeId)
    {
        return noteTypeMapper.selectNoteTypeById(noteTypeId);
    }

    /**
     * 查询游记类型列表
     * 
     * @param noteType 游记类型
     * @return 游记类型
     */
    @Override
    public List<NoteType> selectNoteTypeList(NoteType noteType)
    {
        return noteTypeMapper.selectNoteTypeList(noteType);
    }

    /**
     * 新增游记类型
     * 
     * @param noteType 游记类型
     * @return 结果
     */
    @Override
    public int insertNoteType(NoteType noteType)
    {
        noteType.setCreateTime(DateUtils.getNowDate());
        return noteTypeMapper.insertNoteType(noteType);
    }

    /**
     * 修改游记类型
     * 
     * @param noteType 游记类型
     * @return 结果
     */
    @Override
    public int updateNoteType(NoteType noteType)
    {
        return noteTypeMapper.updateNoteType(noteType);
    }

    /**
     * 删除游记类型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNoteTypeByIds(String ids)
    {
        return noteTypeMapper.deleteNoteTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除游记类型信息
     * 
     * @param noteTypeId 游记类型ID
     * @return 结果
     */
    @Override
    public int deleteNoteTypeById(Long noteTypeId)
    {
        return noteTypeMapper.deleteNoteTypeById(noteTypeId);
    }
}
