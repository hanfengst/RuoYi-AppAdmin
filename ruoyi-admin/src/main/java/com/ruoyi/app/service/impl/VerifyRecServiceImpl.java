package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.VerifyRecMapper;
import com.ruoyi.app.domain.VerifyRec;
import com.ruoyi.app.service.IVerifyRecService;
import com.ruoyi.common.core.text.Convert;

/**
 * 验证码记录Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class VerifyRecServiceImpl implements IVerifyRecService 
{
    @Autowired
    private VerifyRecMapper verifyRecMapper;

    /**
     * 查询验证码记录
     * 
     * @param verifyId 验证码记录ID
     * @return 验证码记录
     */
    @Override
    public VerifyRec selectVerifyRecById(String verifyId)
    {
        return verifyRecMapper.selectVerifyRecById(verifyId);
    }

    /**
     * 查询验证码记录列表
     * 
     * @param verifyRec 验证码记录
     * @return 验证码记录
     */
    @Override
    public List<VerifyRec> selectVerifyRecList(VerifyRec verifyRec)
    {
        return verifyRecMapper.selectVerifyRecList(verifyRec);
    }

    /**
     * 新增验证码记录
     * 
     * @param verifyRec 验证码记录
     * @return 结果
     */
    @Override
    public int insertVerifyRec(VerifyRec verifyRec)
    {
        verifyRec.setCreateTime(DateUtils.getNowDate());
        return verifyRecMapper.insertVerifyRec(verifyRec);
    }

    /**
     * 修改验证码记录
     * 
     * @param verifyRec 验证码记录
     * @return 结果
     */
    @Override
    public int updateVerifyRec(VerifyRec verifyRec)
    {
        return verifyRecMapper.updateVerifyRec(verifyRec);
    }

    /**
     * 删除验证码记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteVerifyRecByIds(String ids)
    {
        return verifyRecMapper.deleteVerifyRecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除验证码记录信息
     * 
     * @param verifyId 验证码记录ID
     * @return 结果
     */
    @Override
    public int deleteVerifyRecById(String verifyId)
    {
        return verifyRecMapper.deleteVerifyRecById(verifyId);
    }
}
