package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.ActCommentTagMapper;
import com.ruoyi.app.domain.ActCommentTag;
import com.ruoyi.app.service.IActCommentTagService;
import com.ruoyi.common.core.text.Convert;

/**
 * 活动评价标签Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class ActCommentTagServiceImpl implements IActCommentTagService 
{
    @Autowired
    private ActCommentTagMapper actCommentTagMapper;

    /**
     * 查询活动评价标签
     * 
     * @param tagId 活动评价标签ID
     * @return 活动评价标签
     */
    @Override
    public ActCommentTag selectActCommentTagById(Long tagId)
    {
        return actCommentTagMapper.selectActCommentTagById(tagId);
    }

    /**
     * 查询活动评价标签列表
     * 
     * @param actCommentTag 活动评价标签
     * @return 活动评价标签
     */
    @Override
    public List<ActCommentTag> selectActCommentTagList(ActCommentTag actCommentTag)
    {
        return actCommentTagMapper.selectActCommentTagList(actCommentTag);
    }

    /**
     * 新增活动评价标签
     * 
     * @param actCommentTag 活动评价标签
     * @return 结果
     */
    @Override
    public int insertActCommentTag(ActCommentTag actCommentTag)
    {
        actCommentTag.setCreateTime(DateUtils.getNowDate());
        return actCommentTagMapper.insertActCommentTag(actCommentTag);
    }

    /**
     * 修改活动评价标签
     * 
     * @param actCommentTag 活动评价标签
     * @return 结果
     */
    @Override
    public int updateActCommentTag(ActCommentTag actCommentTag)
    {
        return actCommentTagMapper.updateActCommentTag(actCommentTag);
    }

    /**
     * 删除活动评价标签对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActCommentTagByIds(String ids)
    {
        return actCommentTagMapper.deleteActCommentTagByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除活动评价标签信息
     * 
     * @param tagId 活动评价标签ID
     * @return 结果
     */
    @Override
    public int deleteActCommentTagById(Long tagId)
    {
        return actCommentTagMapper.deleteActCommentTagById(tagId);
    }
}
