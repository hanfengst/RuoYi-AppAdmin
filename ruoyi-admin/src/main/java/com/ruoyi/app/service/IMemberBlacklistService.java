package com.ruoyi.app.service;

import com.ruoyi.app.domain.MemberBlacklist;
import java.util.List;

/**
 * 会员黑名单Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IMemberBlacklistService 
{
    /**
     * 查询会员黑名单
     * 
     * @param blackId 会员黑名单ID
     * @return 会员黑名单
     */
    public MemberBlacklist selectMemberBlacklistById(Long blackId);

    /**
     * 查询会员黑名单列表
     * 
     * @param memberBlacklist 会员黑名单
     * @return 会员黑名单集合
     */
    public List<MemberBlacklist> selectMemberBlacklistList(MemberBlacklist memberBlacklist);

    /**
     * 新增会员黑名单
     * 
     * @param memberBlacklist 会员黑名单
     * @return 结果
     */
    public int insertMemberBlacklist(MemberBlacklist memberBlacklist);

    /**
     * 修改会员黑名单
     * 
     * @param memberBlacklist 会员黑名单
     * @return 结果
     */
    public int updateMemberBlacklist(MemberBlacklist memberBlacklist);

    /**
     * 批量删除会员黑名单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberBlacklistByIds(String ids);

    /**
     * 删除会员黑名单信息
     * 
     * @param blackId 会员黑名单ID
     * @return 结果
     */
    public int deleteMemberBlacklistById(Long blackId);
}
