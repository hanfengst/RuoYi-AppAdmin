package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.PosterPsdMapper;
import com.ruoyi.app.domain.PosterPsd;
import com.ruoyi.app.service.IPosterPsdService;
import com.ruoyi.common.core.text.Convert;

/**
 * 举报Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class PosterPsdServiceImpl implements IPosterPsdService 
{
    @Autowired
    private PosterPsdMapper posterPsdMapper;

    /**
     * 查询举报
     * 
     * @param psdId 举报ID
     * @return 举报
     */
    @Override
    public PosterPsd selectPosterPsdById(Long psdId)
    {
        return posterPsdMapper.selectPosterPsdById(psdId);
    }

    /**
     * 查询举报列表
     * 
     * @param posterPsd 举报
     * @return 举报
     */
    @Override
    public List<PosterPsd> selectPosterPsdList(PosterPsd posterPsd)
    {
        return posterPsdMapper.selectPosterPsdList(posterPsd);
    }

    /**
     * 新增举报
     * 
     * @param posterPsd 举报
     * @return 结果
     */
    @Override
    public int insertPosterPsd(PosterPsd posterPsd)
    {
        posterPsd.setCreateTime(DateUtils.getNowDate());
        return posterPsdMapper.insertPosterPsd(posterPsd);
    }

    /**
     * 修改举报
     * 
     * @param posterPsd 举报
     * @return 结果
     */
    @Override
    public int updatePosterPsd(PosterPsd posterPsd)
    {
        return posterPsdMapper.updatePosterPsd(posterPsd);
    }

    /**
     * 删除举报对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePosterPsdByIds(String ids)
    {
        return posterPsdMapper.deletePosterPsdByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除举报信息
     * 
     * @param psdId 举报ID
     * @return 结果
     */
    @Override
    public int deletePosterPsdById(Long psdId)
    {
        return posterPsdMapper.deletePosterPsdById(psdId);
    }
}
