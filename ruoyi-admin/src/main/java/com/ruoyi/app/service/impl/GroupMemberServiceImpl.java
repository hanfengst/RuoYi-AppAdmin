package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.GroupMemberMapper;
import com.ruoyi.app.domain.GroupMember;
import com.ruoyi.app.service.IGroupMemberService;
import com.ruoyi.common.core.text.Convert;

/**
 * 加群记录Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class GroupMemberServiceImpl implements IGroupMemberService 
{
    @Autowired
    private GroupMemberMapper groupMemberMapper;

    /**
     * 查询加群记录
     * 
     * @param agmId 加群记录ID
     * @return 加群记录
     */
    @Override
    public GroupMember selectGroupMemberById(String agmId)
    {
        return groupMemberMapper.selectGroupMemberById(agmId);
    }

    /**
     * 查询加群记录列表
     * 
     * @param groupMember 加群记录
     * @return 加群记录
     */
    @Override
    public List<GroupMember> selectGroupMemberList(GroupMember groupMember)
    {
        return groupMemberMapper.selectGroupMemberList(groupMember);
    }

    /**
     * 新增加群记录
     * 
     * @param groupMember 加群记录
     * @return 结果
     */
    @Override
    public int insertGroupMember(GroupMember groupMember)
    {
        groupMember.setCreateTime(DateUtils.getNowDate());
        return groupMemberMapper.insertGroupMember(groupMember);
    }

    /**
     * 修改加群记录
     * 
     * @param groupMember 加群记录
     * @return 结果
     */
    @Override
    public int updateGroupMember(GroupMember groupMember)
    {
        return groupMemberMapper.updateGroupMember(groupMember);
    }

    /**
     * 删除加群记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGroupMemberByIds(String ids)
    {
        return groupMemberMapper.deleteGroupMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除加群记录信息
     * 
     * @param agmId 加群记录ID
     * @return 结果
     */
    @Override
    public int deleteGroupMemberById(String agmId)
    {
        return groupMemberMapper.deleteGroupMemberById(agmId);
    }
}
