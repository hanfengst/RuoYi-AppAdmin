package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.MerchantInfoMapper;
import com.ruoyi.app.domain.MerchantInfo;
import com.ruoyi.app.service.IMerchantInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 商家Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class MerchantInfoServiceImpl implements IMerchantInfoService 
{
    @Autowired
    private MerchantInfoMapper merchantInfoMapper;

    /**
     * 查询商家
     * 
     * @param merId 商家ID
     * @return 商家
     */
    @Override
    public MerchantInfo selectMerchantInfoById(String merId)
    {
        return merchantInfoMapper.selectMerchantInfoById(merId);
    }

    /**
     * 查询商家列表
     * 
     * @param merchantInfo 商家
     * @return 商家
     */
    @Override
    public List<MerchantInfo> selectMerchantInfoList(MerchantInfo merchantInfo)
    {
        return merchantInfoMapper.selectMerchantInfoList(merchantInfo);
    }

    /**
     * 新增商家
     * 
     * @param merchantInfo 商家
     * @return 结果
     */
    @Override
    public int insertMerchantInfo(MerchantInfo merchantInfo)
    {
        merchantInfo.setCreateTime(DateUtils.getNowDate());
        return merchantInfoMapper.insertMerchantInfo(merchantInfo);
    }

    /**
     * 修改商家
     * 
     * @param merchantInfo 商家
     * @return 结果
     */
    @Override
    public int updateMerchantInfo(MerchantInfo merchantInfo)
    {
        return merchantInfoMapper.updateMerchantInfo(merchantInfo);
    }

    /**
     * 删除商家对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMerchantInfoByIds(String ids)
    {
        return merchantInfoMapper.deleteMerchantInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商家信息
     * 
     * @param merId 商家ID
     * @return 结果
     */
    @Override
    public int deleteMerchantInfoById(String merId)
    {
        return merchantInfoMapper.deleteMerchantInfoById(merId);
    }
}
