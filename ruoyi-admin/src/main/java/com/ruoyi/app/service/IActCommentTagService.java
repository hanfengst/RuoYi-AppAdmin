package com.ruoyi.app.service;

import com.ruoyi.app.domain.ActCommentTag;
import java.util.List;

/**
 * 活动评价标签Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IActCommentTagService 
{
    /**
     * 查询活动评价标签
     * 
     * @param tagId 活动评价标签ID
     * @return 活动评价标签
     */
    public ActCommentTag selectActCommentTagById(Long tagId);

    /**
     * 查询活动评价标签列表
     * 
     * @param actCommentTag 活动评价标签
     * @return 活动评价标签集合
     */
    public List<ActCommentTag> selectActCommentTagList(ActCommentTag actCommentTag);

    /**
     * 新增活动评价标签
     * 
     * @param actCommentTag 活动评价标签
     * @return 结果
     */
    public int insertActCommentTag(ActCommentTag actCommentTag);

    /**
     * 修改活动评价标签
     * 
     * @param actCommentTag 活动评价标签
     * @return 结果
     */
    public int updateActCommentTag(ActCommentTag actCommentTag);

    /**
     * 批量删除活动评价标签
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActCommentTagByIds(String ids);

    /**
     * 删除活动评价标签信息
     * 
     * @param tagId 活动评价标签ID
     * @return 结果
     */
    public int deleteActCommentTagById(Long tagId);
}
