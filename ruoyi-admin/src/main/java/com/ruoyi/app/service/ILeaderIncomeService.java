package com.ruoyi.app.service;

import com.ruoyi.app.domain.LeaderIncome;
import java.util.List;

/**
 * 领队收入Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface ILeaderIncomeService 
{
    /**
     * 查询领队收入
     * 
     * @param leaderIncomeId 领队收入ID
     * @return 领队收入
     */
    public LeaderIncome selectLeaderIncomeById(String leaderIncomeId);

    /**
     * 查询领队收入列表
     * 
     * @param leaderIncome 领队收入
     * @return 领队收入集合
     */
    public List<LeaderIncome> selectLeaderIncomeList(LeaderIncome leaderIncome);

    /**
     * 新增领队收入
     * 
     * @param leaderIncome 领队收入
     * @return 结果
     */
    public int insertLeaderIncome(LeaderIncome leaderIncome);

    /**
     * 修改领队收入
     * 
     * @param leaderIncome 领队收入
     * @return 结果
     */
    public int updateLeaderIncome(LeaderIncome leaderIncome);

    /**
     * 批量删除领队收入
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteLeaderIncomeByIds(String ids);

    /**
     * 删除领队收入信息
     * 
     * @param leaderIncomeId 领队收入ID
     * @return 结果
     */
    public int deleteLeaderIncomeById(String leaderIncomeId);
}
