package com.ruoyi.app.service;

import com.ruoyi.app.domain.MessageRec;
import java.util.List;

/**
 * 消息记录Service接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface IMessageRecService 
{
    /**
     * 查询消息记录
     * 
     * @param msgId 消息记录ID
     * @return 消息记录
     */
    public MessageRec selectMessageRecById(String msgId);

    /**
     * 查询消息记录列表
     * 
     * @param messageRec 消息记录
     * @return 消息记录集合
     */
    public List<MessageRec> selectMessageRecList(MessageRec messageRec);

    /**
     * 新增消息记录
     * 
     * @param messageRec 消息记录
     * @return 结果
     */
    public int insertMessageRec(MessageRec messageRec);

    /**
     * 修改消息记录
     * 
     * @param messageRec 消息记录
     * @return 结果
     */
    public int updateMessageRec(MessageRec messageRec);

    /**
     * 批量删除消息记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMessageRecByIds(String ids);

    /**
     * 删除消息记录信息
     * 
     * @param msgId 消息记录ID
     * @return 结果
     */
    public int deleteMessageRecById(String msgId);
}
