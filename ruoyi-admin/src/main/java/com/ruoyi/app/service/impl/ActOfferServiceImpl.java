package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.ActOfferMapper;
import com.ruoyi.app.domain.ActOffer;
import com.ruoyi.app.service.IActOfferService;
import com.ruoyi.common.core.text.Convert;

/**
 * 活动邀约Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class ActOfferServiceImpl implements IActOfferService 
{
    @Autowired
    private ActOfferMapper actOfferMapper;

    /**
     * 查询活动邀约
     * 
     * @param offerId 活动邀约ID
     * @return 活动邀约
     */
    @Override
    public ActOffer selectActOfferById(String offerId)
    {
        return actOfferMapper.selectActOfferById(offerId);
    }

    /**
     * 查询活动邀约列表
     * 
     * @param actOffer 活动邀约
     * @return 活动邀约
     */
    @Override
    public List<ActOffer> selectActOfferList(ActOffer actOffer)
    {
        return actOfferMapper.selectActOfferList(actOffer);
    }

    /**
     * 新增活动邀约
     * 
     * @param actOffer 活动邀约
     * @return 结果
     */
    @Override
    public int insertActOffer(ActOffer actOffer)
    {
        actOffer.setCreateTime(DateUtils.getNowDate());
        return actOfferMapper.insertActOffer(actOffer);
    }

    /**
     * 修改活动邀约
     * 
     * @param actOffer 活动邀约
     * @return 结果
     */
    @Override
    public int updateActOffer(ActOffer actOffer)
    {
        return actOfferMapper.updateActOffer(actOffer);
    }

    /**
     * 删除活动邀约对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActOfferByIds(String ids)
    {
        return actOfferMapper.deleteActOfferByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除活动邀约信息
     * 
     * @param offerId 活动邀约ID
     * @return 结果
     */
    @Override
    public int deleteActOfferById(String offerId)
    {
        return actOfferMapper.deleteActOfferById(offerId);
    }
}
