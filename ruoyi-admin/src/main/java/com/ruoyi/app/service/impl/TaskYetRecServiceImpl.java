package com.ruoyi.app.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app.mapper.TaskYetRecMapper;
import com.ruoyi.app.domain.TaskYetRec;
import com.ruoyi.app.service.ITaskYetRecService;
import com.ruoyi.common.core.text.Convert;

/**
 * 任务记录Service业务层处理
 * 
 * @author bigStream
 * @date 2020-08-02
 */
@Service
public class TaskYetRecServiceImpl implements ITaskYetRecService 
{
    @Autowired
    private TaskYetRecMapper taskYetRecMapper;

    /**
     * 查询任务记录
     * 
     * @param yetId 任务记录ID
     * @return 任务记录
     */
    @Override
    public TaskYetRec selectTaskYetRecById(String yetId)
    {
        return taskYetRecMapper.selectTaskYetRecById(yetId);
    }

    /**
     * 查询任务记录列表
     * 
     * @param taskYetRec 任务记录
     * @return 任务记录
     */
    @Override
    public List<TaskYetRec> selectTaskYetRecList(TaskYetRec taskYetRec)
    {
        return taskYetRecMapper.selectTaskYetRecList(taskYetRec);
    }

    /**
     * 新增任务记录
     * 
     * @param taskYetRec 任务记录
     * @return 结果
     */
    @Override
    public int insertTaskYetRec(TaskYetRec taskYetRec)
    {
        taskYetRec.setCreateTime(DateUtils.getNowDate());
        return taskYetRecMapper.insertTaskYetRec(taskYetRec);
    }

    /**
     * 修改任务记录
     * 
     * @param taskYetRec 任务记录
     * @return 结果
     */
    @Override
    public int updateTaskYetRec(TaskYetRec taskYetRec)
    {
        return taskYetRecMapper.updateTaskYetRec(taskYetRec);
    }

    /**
     * 删除任务记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTaskYetRecByIds(String ids)
    {
        return taskYetRecMapper.deleteTaskYetRecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除任务记录信息
     * 
     * @param yetId 任务记录ID
     * @return 结果
     */
    @Override
    public int deleteTaskYetRecById(String yetId)
    {
        return taskYetRecMapper.deleteTaskYetRecById(yetId);
    }
}
