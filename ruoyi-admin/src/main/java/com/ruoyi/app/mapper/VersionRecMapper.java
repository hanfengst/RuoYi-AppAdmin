package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.VersionRec;
import java.util.List;

/**
 * app版本Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface VersionRecMapper 
{
    /**
     * 查询app版本
     * 
     * @param verId app版本ID
     * @return app版本
     */
    public VersionRec selectVersionRecById(String verId);

    /**
     * 查询app版本列表
     * 
     * @param versionRec app版本
     * @return app版本集合
     */
    public List<VersionRec> selectVersionRecList(VersionRec versionRec);

    /**
     * 新增app版本
     * 
     * @param versionRec app版本
     * @return 结果
     */
    public int insertVersionRec(VersionRec versionRec);

    /**
     * 修改app版本
     * 
     * @param versionRec app版本
     * @return 结果
     */
    public int updateVersionRec(VersionRec versionRec);

    /**
     * 删除app版本
     * 
     * @param verId app版本ID
     * @return 结果
     */
    public int deleteVersionRecById(String verId);

    /**
     * 批量删除app版本
     * 
     * @param verIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteVersionRecByIds(String[] verIds);
}
