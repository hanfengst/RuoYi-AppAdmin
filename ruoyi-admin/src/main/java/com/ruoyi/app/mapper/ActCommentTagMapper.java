package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.ActCommentTag;
import java.util.List;

/**
 * 活动评价标签Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface ActCommentTagMapper 
{
    /**
     * 查询活动评价标签
     * 
     * @param tagId 活动评价标签ID
     * @return 活动评价标签
     */
    public ActCommentTag selectActCommentTagById(Long tagId);

    /**
     * 查询活动评价标签列表
     * 
     * @param actCommentTag 活动评价标签
     * @return 活动评价标签集合
     */
    public List<ActCommentTag> selectActCommentTagList(ActCommentTag actCommentTag);

    /**
     * 新增活动评价标签
     * 
     * @param actCommentTag 活动评价标签
     * @return 结果
     */
    public int insertActCommentTag(ActCommentTag actCommentTag);

    /**
     * 修改活动评价标签
     * 
     * @param actCommentTag 活动评价标签
     * @return 结果
     */
    public int updateActCommentTag(ActCommentTag actCommentTag);

    /**
     * 删除活动评价标签
     * 
     * @param tagId 活动评价标签ID
     * @return 结果
     */
    public int deleteActCommentTagById(Long tagId);

    /**
     * 批量删除活动评价标签
     * 
     * @param tagIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteActCommentTagByIds(String[] tagIds);
}
