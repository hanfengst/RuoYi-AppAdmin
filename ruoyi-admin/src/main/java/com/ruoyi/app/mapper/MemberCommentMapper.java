package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.MemberComment;
import java.util.List;

/**
 * 会员评价Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface MemberCommentMapper 
{
    /**
     * 查询会员评价
     * 
     * @param memCommId 会员评价ID
     * @return 会员评价
     */
    public MemberComment selectMemberCommentById(Long memCommId);

    /**
     * 查询会员评价列表
     * 
     * @param memberComment 会员评价
     * @return 会员评价集合
     */
    public List<MemberComment> selectMemberCommentList(MemberComment memberComment);

    /**
     * 新增会员评价
     * 
     * @param memberComment 会员评价
     * @return 结果
     */
    public int insertMemberComment(MemberComment memberComment);

    /**
     * 修改会员评价
     * 
     * @param memberComment 会员评价
     * @return 结果
     */
    public int updateMemberComment(MemberComment memberComment);

    /**
     * 删除会员评价
     * 
     * @param memCommId 会员评价ID
     * @return 结果
     */
    public int deleteMemberCommentById(Long memCommId);

    /**
     * 批量删除会员评价
     * 
     * @param memCommIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberCommentByIds(String[] memCommIds);
}
