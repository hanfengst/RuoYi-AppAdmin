package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.GroupType;
import java.util.List;

/**
 * 群类型Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface GroupTypeMapper 
{
    /**
     * 查询群类型
     * 
     * @param groupTypeId 群类型ID
     * @return 群类型
     */
    public GroupType selectGroupTypeById(Long groupTypeId);

    /**
     * 查询群类型列表
     * 
     * @param groupType 群类型
     * @return 群类型集合
     */
    public List<GroupType> selectGroupTypeList(GroupType groupType);

    /**
     * 新增群类型
     * 
     * @param groupType 群类型
     * @return 结果
     */
    public int insertGroupType(GroupType groupType);

    /**
     * 修改群类型
     * 
     * @param groupType 群类型
     * @return 结果
     */
    public int updateGroupType(GroupType groupType);

    /**
     * 删除群类型
     * 
     * @param groupTypeId 群类型ID
     * @return 结果
     */
    public int deleteGroupTypeById(Long groupTypeId);

    /**
     * 批量删除群类型
     * 
     * @param groupTypeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupTypeByIds(String[] groupTypeIds);
}
