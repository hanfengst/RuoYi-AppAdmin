package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.MerchantIncome;
import java.util.List;

/**
 * 推荐商户收入Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface MerchantIncomeMapper 
{
    /**
     * 查询推荐商户收入
     * 
     * @param merIncomeId 推荐商户收入ID
     * @return 推荐商户收入
     */
    public MerchantIncome selectMerchantIncomeById(String merIncomeId);

    /**
     * 查询推荐商户收入列表
     * 
     * @param merchantIncome 推荐商户收入
     * @return 推荐商户收入集合
     */
    public List<MerchantIncome> selectMerchantIncomeList(MerchantIncome merchantIncome);

    /**
     * 新增推荐商户收入
     * 
     * @param merchantIncome 推荐商户收入
     * @return 结果
     */
    public int insertMerchantIncome(MerchantIncome merchantIncome);

    /**
     * 修改推荐商户收入
     * 
     * @param merchantIncome 推荐商户收入
     * @return 结果
     */
    public int updateMerchantIncome(MerchantIncome merchantIncome);

    /**
     * 删除推荐商户收入
     * 
     * @param merIncomeId 推荐商户收入ID
     * @return 结果
     */
    public int deleteMerchantIncomeById(String merIncomeId);

    /**
     * 批量删除推荐商户收入
     * 
     * @param merIncomeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMerchantIncomeByIds(String[] merIncomeIds);
}
