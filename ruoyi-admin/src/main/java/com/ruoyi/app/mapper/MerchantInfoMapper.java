package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.MerchantInfo;
import java.util.List;

/**
 * 商家Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface MerchantInfoMapper 
{
    /**
     * 查询商家
     * 
     * @param merId 商家ID
     * @return 商家
     */
    public MerchantInfo selectMerchantInfoById(String merId);

    /**
     * 查询商家列表
     * 
     * @param merchantInfo 商家
     * @return 商家集合
     */
    public List<MerchantInfo> selectMerchantInfoList(MerchantInfo merchantInfo);

    /**
     * 新增商家
     * 
     * @param merchantInfo 商家
     * @return 结果
     */
    public int insertMerchantInfo(MerchantInfo merchantInfo);

    /**
     * 修改商家
     * 
     * @param merchantInfo 商家
     * @return 结果
     */
    public int updateMerchantInfo(MerchantInfo merchantInfo);

    /**
     * 删除商家
     * 
     * @param merId 商家ID
     * @return 结果
     */
    public int deleteMerchantInfoById(String merId);

    /**
     * 批量删除商家
     * 
     * @param merIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMerchantInfoByIds(String[] merIds);
}
