package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.Advert;
import java.util.List;

/**
 * 广告Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface AdvertMapper 
{
    /**
     * 查询广告
     * 
     * @param adId 广告ID
     * @return 广告
     */
    public Advert selectAdvertById(Long adId);

    /**
     * 查询广告列表
     * 
     * @param advert 广告
     * @return 广告集合
     */
    public List<Advert> selectAdvertList(Advert advert);

    /**
     * 新增广告
     * 
     * @param advert 广告
     * @return 结果
     */
    public int insertAdvert(Advert advert);

    /**
     * 修改广告
     * 
     * @param advert 广告
     * @return 结果
     */
    public int updateAdvert(Advert advert);

    /**
     * 删除广告
     * 
     * @param adId 广告ID
     * @return 结果
     */
    public int deleteAdvertById(Long adId);

    /**
     * 批量删除广告
     * 
     * @param adIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteAdvertByIds(String[] adIds);
}
