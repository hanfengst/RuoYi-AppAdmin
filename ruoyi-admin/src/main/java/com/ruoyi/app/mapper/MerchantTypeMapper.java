package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.MerchantType;
import java.util.List;

/**
 * 商家分类Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface MerchantTypeMapper 
{
    /**
     * 查询商家分类
     * 
     * @param merTypeId 商家分类ID
     * @return 商家分类
     */
    public MerchantType selectMerchantTypeById(Long merTypeId);

    /**
     * 查询商家分类列表
     * 
     * @param merchantType 商家分类
     * @return 商家分类集合
     */
    public List<MerchantType> selectMerchantTypeList(MerchantType merchantType);

    /**
     * 新增商家分类
     * 
     * @param merchantType 商家分类
     * @return 结果
     */
    public int insertMerchantType(MerchantType merchantType);

    /**
     * 修改商家分类
     * 
     * @param merchantType 商家分类
     * @return 结果
     */
    public int updateMerchantType(MerchantType merchantType);

    /**
     * 删除商家分类
     * 
     * @param merTypeId 商家分类ID
     * @return 结果
     */
    public int deleteMerchantTypeById(Long merTypeId);

    /**
     * 批量删除商家分类
     * 
     * @param merTypeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteMerchantTypeByIds(String[] merTypeIds);
}
