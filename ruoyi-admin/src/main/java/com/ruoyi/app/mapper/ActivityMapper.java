package com.ruoyi.app.mapper;

import com.ruoyi.app.domain.Activity;
import java.util.List;

/**
 * 活动Mapper接口
 * 
 * @author bigStream
 * @date 2020-08-02
 */
public interface ActivityMapper 
{
    /**
     * 查询活动
     * 
     * @param actId 活动ID
     * @return 活动
     */
    public Activity selectActivityById(String actId);

    /**
     * 查询活动列表
     * 
     * @param activity 活动
     * @return 活动集合
     */
    public List<Activity> selectActivityList(Activity activity);

    /**
     * 新增活动
     * 
     * @param activity 活动
     * @return 结果
     */
    public int insertActivity(Activity activity);

    /**
     * 修改活动
     * 
     * @param activity 活动
     * @return 结果
     */
    public int updateActivity(Activity activity);

    /**
     * 删除活动
     * 
     * @param actId 活动ID
     * @return 结果
     */
    public int deleteActivityById(String actId);

    /**
     * 批量删除活动
     * 
     * @param actIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteActivityByIds(String[] actIds);
}
