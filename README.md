## 功能模块

#### 本项目是【小猪趣聚APP】后台运营管理系统
支持在线发布活动，参加兴趣社群，发布活动感悟回放，搜索附件的人，基于webSocket开发的聊天系统，商户入驻，商家发放优惠券，用户可领券去线下消费，发布旅游笔记

分享活动赚取佣金，广场动态，会员积分登记制度，裂变分享；粉丝，关注，收藏等功能。理想很丰满，现实很骨感，写这个项目当做练手学习，最后商业逻辑没跑通；定位不清晰等原因；
停止运营了（也可以说一开始就没正式运营，只是小圈子内推广试用）

## 手机端页面截图（后台截图请运行此项目）

<table>
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/1.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/2.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/3.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/4.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/5.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/6.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/7.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/8.jpg"/></td>
    </tr>	 
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/9.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/10.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/11.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/12.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/13.jpg"/></td>
        <td><img src="https://gitee.com/xww520/quju-app/raw/dev-v5/screenshot/14.jpg"/></td>
    </tr>
</table>

## 平台简介

为什么还开发【小猪趣聚】社交APP？（项目已停服，现全部贡献源码）

据数据调查显示，目前城市单身青年想要脱单现状：
下载注册各种相亲或社交APP，纯线上模式，变相充值或购买服务，但还会遇盗各种虚假会员和托；（网聊不靠谱，充值服务费用高）
参加相亲机构组织策划的脱单活动；（费用高，相亲太直接，部分人员信息不对等，如年龄，职业，收入，学历等）
进自媒体或微博号提供的各种收费单身微信群；（群里成员参差不齐，还收费；没人组织线下活动）
加入各种兴趣爱好群；（加的群多，潜水的多，出去线下见面认识的少）
同学圈子，同事圈子，家人圈子，因这些圈子都有限，脱单效率低下；

【小猪趣聚】正逐一解决以上问题而开发的一款新社交软件，通过
线上大数据AI匹配对象，线下撮合聚会，让大数据在相亲社交领域赋能，减少无效无意义社交！

使命：
互帮互助，众人拾柴火焰高，帮助城市单身青年，扩大社交圈子，认识更多异性和朋友，告别枯躁乏味的宅家时间，加入更多线下吃喝玩乐兴趣活动，让每个人的生活多一份乐趣！趣生活，趣同城！
愿景：
做中国最大的纯净真实，公益免费，非盈利性交友平台；让相亲找对象这事变得更简单，我为人人，人人为我，互帮互助，公益免费；
价值观：
真诚友善，我为人人，人人为我，互帮互助；

## 技术简介

包括vue前端，springboot的服务端，和springmvc的后台运营管理系统，后续将逐步开源，希望有需的朋友可以使用或参考此源码；
基于springboot快速开发框架，基础上修改的适用于app，小程序，h5等客户端的后台api接口系统；
集成jwt和阿里云SSO对象存储服务，nginx做动静分离，直接访问阿里云sso上的图片资源等静态资源；动态接口则访问本工程；
适合需要二次开发app或小程序接口，可参考此项目；

* 如需前端项目请查看链接 ，请移步 [quju-app](https://gitee.com/xww520/quju-app)
* 如需小猪趣聚的后台运营管理系统  [AppAdmin](https://gitee.com/xww520/RuoYi-AppAdmin)
* 如需API后端服务接口项目请查看链接 ，请移步 [AppApi](https://gitee.com/xww520/RuoYi-AppApi)
* 【毫无保留给个人及企业免费使用；拥抱开源，让技术实现更多的价值】

## 内置功能

1. 支持自动生成restful api接口；
2. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
3. 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
4. 岗位管理：配置系统用户所属担任职务。
5. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
6. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
7. 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
8. 参数管理：对系统动态配置常用参数。
9. 通知公告：系统通知公告信息发布维护。
10. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
11. 登录日志：系统登录日志记录查询包含登录异常。
12. 在线用户：当前系统中活跃用户状态监控。
13. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
14. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
15. 系统接口：根据业务代码自动生成相关的api接口文档。
16. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
17. 在线构建器：拖动表单元素生成相应的HTML代码。
18. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 文件说明

> 建模文件和sql脚本在/sql/xxxx.pdm
> 整体的系统框架模块
>
> 账号密码是：admin/admin123
